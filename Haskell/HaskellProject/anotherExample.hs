
-- Corha Gratiela

-- the exercises 9, 11, 12, 16 don't work properly; the others are good
-- these example may not be the most recommended solutions, especially where recursion is not envolved, but are easier to understand and implement :D  (from 7 onwards)

import Data.List

data STree a = Nil | Node a (STree a) (STree a)
 deriving (Show, Eq)
 

-- examples
tree1 = Nil

tree2 = 
 Node 2
  (Node 5 (Node 7 Nil Nil) Nil)
  (Node 20 (Node 26 Nil Nil) Nil)
  
tree3 = 
 Node 2
  (Node 1 Nil Nil)
  (Node 4 Nil (Node 6 Nil Nil))
  
-- lt = leftTree
-- rt = rightTree

-- 1
nil :: STree a
nil = Nil

-- 2
isNil :: STree a -> Bool
isNil Nil = True
isNil _ = False
 
-- 3 
isNode :: STree a -> Bool
isNode t = not (isNil t)


-- 4
left :: STree a -> STree a
left Nil = error ("Tree is nil!")
left (Node _ lt rt) = lt

right :: STree a -> STree a
right Nil = error ("Tree is nil!")
right (Node _ lt rt) = rt


-- 5
val :: STree a -> a
val Nil = error ("Tree is nil!")
val (Node x _ _) = x


-- 6
size :: STree a -> Int
size Nil = 0
size (Node _ lt Nil) = 1 + (size lt)
size (Node _ Nil rt) = 1 + (size rt)
size (Node _ lt rt) = 1 + (size lt) + (size rt)


-- 7
isOrdered1 :: Ord a => STree a -> Bool
isOrdered1 Nil = True
isOrdered1 t = (isOrdered2 (inorder t))


-- 8
isOrdered2 :: Ord a => [a] -> Bool
isOrdered2 [] = True
isOrdered2 [x] = True
isOrdered2 (x:xs)
 | (x < (minimum xs) ) = (isOrdered2 xs)
 | (x > (minimum xs) ) = False
 

-- 9 (Problem)
indexTree :: Int -> STree a -> (Maybe a)
indexTree n Nil = Nothing
-- indexTree n t = inorder t
indexTree n (Node v lt rt)
 | (n > (size (Node v lt rt))) = Nothing
 | (n == 0) = (Just v)
 | otherwise = (indexTree (n-1) lt) + (indexTree (n-1) rt)
 

-- 10
inorder :: STree a -> [a]
inorder Nil = []
inorder (Node v lt rt) = (inorder lt) ++ [v] ++ (inorder rt)


-- 11 (Problem)
insTree :: Ord a => a -> STree a -> STree a
insTree v Nil = (Node v Nil Nil)
insTree v (Node x lt rt)
 | (x == v) = (Node x lt rt)
 | (v < x) = (Node x (insTree v lt) rt)
 | (v > x) = (Node x lt (insTree v rt))
 
 
-- 12 (Problem)
delTree :: Ord a => a -> STree a -> STree a
delTree v Nil = Nil
delTree v (Node x lt rt)
 | (x == v) = (Node x lt rt)
 | (v < x) = (Node x (delTree v lt) rt)
 | (v > x) = (Node x lt (delTree v rt))


-- 13
minVal :: Ord a => STree a -> Maybe a
minVal Nil = Nothing
minVal t = (Just min) where
 min = minimum (inorder t)
 
maxVal :: Ord a => STree a -> Maybe a
maxVal Nil = Nothing
maxVal t = (Just max) where
 max = maximum (inorder t)
 

-- 14
bSort :: Ord a => STree a -> [a]
bSort Nil = []
bSort t = sort (inorder t)


-- 15
successor :: Ord a => a -> STree a -> Maybe a
successor v Nil = Nothing
successor v t = (Just min) where
 min = minimum (filter (>v) (inorder t))


-- 16 (Problem)
closest :: Ord a => a -> STree a -> Maybe a
closest v (Node x Nil Nil) = Just x
closest v t = (Just min) where
 min = minimum (map (abs(\x -> x-v)) (inorder t))


-- 17
between :: (Ord a) =>  a -> a -> STree a -> [a]
between v1 v2 Nil = []
between v1 v2 t = 
 filter ( \x -> ((x > v1) && (x < v2)) ) (inorder t)

