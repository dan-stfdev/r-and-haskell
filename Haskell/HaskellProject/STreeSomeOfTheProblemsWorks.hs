module STree
 (STree,
   nil, -- STree a
   isNil, -- STree a -> Bool
   isNode, -- STree a -> Bool
   left, -- STree a -> STree a
   right, -- STree a -> STree a
   val, -- STree a -> STree a
   size, -- STree a -> Int
   isOrdered, -- Ord a => STree a -> Bool
   isOrdered2, -- Ord a => [a] -> Bool
   isOrdered4, -- Ord a => [a] -> Bool
   inorder, -- STree a -> [a]
   indexTree, -- Int -> Tree a -> Maybe a
   insert, -- Ord a => a -> STree a -> STree a
   delTree, -- Ord a => a -> STree a -> STree a
   minVal, -- Ord a => a -> STree a -> Maybe a
   maxVal, -- Ord a => a -> STree a -> Maybe a
   bSort, -- Ord a => [a] -> [a]
   successor, -- Ord a => a -> STree a -> Maybe a
   closest, -- Ord a => a -> STree a -> Maybe a
   between -- Ord a => a -> a -> STree a -> [a]
  ) 
where

import Data.List (sort)
import Data.Tree ()

data STree a = Nil | Node a (STree a) (STree a) deriving (Show,Eq)

-- examples
tree1 = Nil

tree2 = 
 Node 2
  (Node 7 (Node 8 Nil Nil) Nil)
  (Node 20 (Node 26 Nil Nil) Nil)
  
tree3 = 
 Node 2
  (Node 1 Nil Nil)
  (Node 4 Nil (Node 6 Nil Nil))
  
-- lt = leftTree
-- rt = rightTree

------------------------------------------------------------------------1
nil :: STree a
nil = Nil
-- type nil
------------------------------------------------------------------------2
isNil :: STree a -> Bool
isNil Nil = True
isNil _ = False
-- type isNil nil
-- type isNil tree3
------------------------------------------------------------------------3
isNode :: STree a -> Bool
isNode t = not (isNil t)
-- type isNode nil
-- type isNode tree2
------------------------------------------------------------------------4
left :: STree a -> STree a
left Nil = error "Tree is nil!"
left (Node _ lt rt) = lt

right :: STree a -> STree a
right Nil = error "Tree is nil!"
right (Node _ lt rt) = rt

--type left tree1
--type right tree2

------------------------------------------------------------------------5
val :: STree a -> a
val Nil = error "Tree is nil!"
val (Node x _ _) = x
--type val tree1
--type val tree2

------------------------------------------------------------------------6

size :: STree a -> Int
size Nil = 0
size (Node _ lt Nil) = 1 + size lt
size (Node _ Nil rt) = 1 + size rt
size (Node _ lt rt) = 1 + size lt + size rt
--type size tree1
--type size tree2

------------------------------------------------------------------------8
isOrdered2 :: Ord a => [a] -> Bool
isOrdered2 [] = True
isOrdered2 [x] = True
isOrdered2 (x:xs)
 | x < minimum xs = isOrdered2 xs
 | x > minimum xs  = False


--or

isOrdered4 :: Ord a => [a] -> Bool
isOrdered4 (x:y:xs) | x<=y = isOrdered4 (y:xs)
                   | otherwise = False
isOrdered4 _ = True

--type isOrdered2 [2,3,4]
--type isOrdered4 [2,3,4]
------------------------------------------------------------------------10
--le ordoneaza 
inorder :: (Ord a) => STree a -> [a]
inorder Nil = []
inorder (Node t1 x t2) = inorder x ++ (t1 : inorder t2)

--type inorder tree3
------------------------------------------------------------------------7
isOrdered :: (Ord a) => STree a -> Bool
isOrdered Nil = True
isOrdered (Node l x r) = checkIfTheListOnTheLeftisOrder (leftList x) && checkIfTheListOnTheRightisOrder (rightList x) && isOrdered2(leftList x) && isOrdered2(rightList x)
--type isOrdered tree2

-- it doesn't work properly because it takes the root all the time and I don;t know how to make it work without it -- it is not good drop 1 (leftList x)
leftList  :: (Ord a) => STree a -> [a]
leftList Nil = []
leftList (Node t1 x t2) = t1 : leftList x
--if we have Nil after the first Node see in tree3 it will not return what we need to be returned -> 2 1 6
 
rightList  :: (Ord a) => STree a -> [a]
rightList Nil = []
rightList (Node l x r) = l : leftList r ++ rightList x
-- if we have Nil after the first Node in the right part we should check  (l : leftList r) 

-- if we have a maximum in that list another one different than first element than this means that we have at least one element bigger than the head of the list
checkIfTheListOnTheLeftisOrder :: Ord a => [a] -> Bool
checkIfTheListOnTheLeftisOrder [] = True
checkIfTheListOnTheLeftisOrder (x:xs)
  | head xs > maximum xs = True
  | otherwise = False


-- if we have a minimum in that list another one different than first element than this means that we have at least one element smaller than the head of the list
checkIfTheListOnTheRightisOrder :: Ord a => [a] -> Bool
checkIfTheListOnTheRightisOrder [] = True
checkIfTheListOnTheRightisOrder (x:xs)
  | head xs > minimum xs = True
  | otherwise = False

------------------------------------------------------------------------9 
indexTree :: Int -> STree a -> Maybe a
-- indexTree n t = inorder t
indexTree n t
  | isNil t = Nothing
  | n < st1 = indexTree n t1
  | n == st1 = Just v
  | otherwise = indexTree (n-st1-1) t2
    where 
      v = val t
      t1 = left t
      t2 = right t
      st1 = size t1

--type indexTree 2 tree2
------------------------------------------------------------------------12
delTree :: Ord a => a -> STree a -> STree a
delTree v Nil = Nil
delTree v (Node x lt rt)
 | x == v = Node x lt rt
 | v < x = Node x (delTree v lt) rt
 | v > x = Node x lt (delTree v rt)

--type delTree 2 tree3
------------------------------------------------------------------------11
--insert = insTree
insert :: (Ord a) => a -> STree a -> STree a
insert x Nil = Node x Nil Nil
insert x (Node v l r) 
  | v == x = Node x l r
  | x < v = Node v (insert x l) r
  | x > v = Node v l (insert x r)

-- type insert 5 tree2
 ------------------------------------------------------------------------13
minVal :: Ord a => STree a -> Maybe a
minVal Nil = Nothing
minVal t = Just min where
 min = minimum (inorder t)
 
maxVal :: Ord a => STree a -> Maybe a
maxVal Nil = Nothing
maxVal t = Just max where
 max = maximum (inorder t)

--type minVal tree2
--type maxVal tree2

------------------------------------------------------------------------14
bSort :: Ord a => STree a -> [a]
bSort Nil = []
bSort t = sort (inorder t)

--type bSort tree3

------------------------------------------------------------------------15
successor :: Ord a => a -> STree a -> Maybe a
successor v Nil = Nothing
successor v t = Just min where
 min = minimum (filter (>v) (inorder t))

--type successor 4 tree3
------------------------------------------------------------------------16
closest :: Ord a => a -> STree a -> Maybe a
closest v (Node x Nil Nil) = Just x
closest v (Node t1 y t2) = Just min 
 where
      min = minimum (map((\x -> x-v) (inorder y)))
      -- it should check for each elem of the ordered stree the diff btw elem that we search and take the minimum from them but it doesn't work

------------------------------------------------------------------------17
--btw nodes
between :: (Ord a) =>  a -> a -> STree a -> [a]
between v1 v2 Nil = []
between v1 v2 t = 
 filter ( \x -> (x > v1) && (x < v2) ) (inorder t)

-- type between 1 3 tree3