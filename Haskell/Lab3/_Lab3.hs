-----------------------------------------------------------------------
--  Haskell: The Craft of Functional Programming
--  Simon Thompson
--  (c) Addison-Wesley, 1996-2010.
--
--  Pictures.hs
-- 
--     An implementation of a type of rectangular pictures  
--     using lists of lists of characters. 
-----------------------------------------------------------------------



-- The basics
-- ^^^^^^^^^^
module Lab3 where
import Data.List --(transpose)
-- import Test.QuickCheck


type Picture = [[Char]]

-- The example used in Craft2e: a polygon which looks like a horse. Here
-- taken to be a 16 by 12 rectangle.

horse :: Picture

horse = [".......##...",
         ".....##..#..",
         "...##.....#.",
         "..#.......#.",
         "..#...#...#.",
         "..#...###.#.",
         ".#....#..##.",
         "..#...#.....",
         "...#...#....",
         "....#..#....",
         ".....#.#....",
         "......##...."]

-- Completely white and black pictures.

white :: Picture

white = ["......",
         "......",
         "......",
         "......",
         "......",
         "......"]

black :: Picture

black = ["######",
         "######",
         "######",
         "######",
         "######",
         "######"]

-- Getting a picture onto the screen.

printPicture :: Picture -> IO ()

printPicture = putStr . concat . map (++"\n")


-- Transformations of pictures.
-- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^

-- Reflection in a vertical mirror.

flipV :: Picture -> Picture

flipV = map reverse

-- Reflection in a horizontal mirror.

flipH :: Picture -> Picture

flipH = reverse

-- Rotation through 180 degrees, by composing vertical and horizontal
-- reflection. Note that it can also be done by flipV.flipH, and that we
-- can prove equality of the two functions.

rotate :: Picture -> Picture

rotate = flipH . flipV

-- One picture above another. To maintain the rectangular property,
-- the pictures need to have the same width.

above :: Picture -> Picture -> Picture

above = (++)

-- One picture next to another. To maintain the rectangular property,
-- the pictures need to have the same height.

beside :: Picture -> Picture -> Picture

beside = zipWith (++)

-- Superimose one picture above another. Assume the pictures to be the same
-- size. The individual characters are combined using the combine function.

superimpose :: Picture -> Picture -> Picture

superimpose = zipWith (zipWith combine)

-- For the result to be '.' both components have to the '.'; otherwise
-- get the '#' character.

combine :: Char -> Char -> Char

combine topCh bottomCh
  = if (topCh == '.' && bottomCh == '.') 
    then '.'
    else '#'

-- Inverting the colours in a picture; done pointwise by invert...

invertColour :: Picture -> Picture

invertColour = map (map invert)

-- ... which works by making the result '.' unless the input is '.'.

invert :: Char -> Char

invert ch = if ch == '.' then '#' else '.'


-- Property

prop_rotate, prop_flipV, prop_flipH :: Picture -> Bool

prop_rotate pic = flipV (flipH pic) == flipH (flipV pic)

prop_flipV pic = flipV (flipV pic) == pic

prop_flipH pic = flipH (flipV pic) == pic

test_rotate, test_flipV, test_flipH :: Bool
 
test_rotate = flipV (flipH horse) == flipH (flipV horse)

test_flipV = flipV (flipV horse) == horse

test_flipH = flipH (flipV horse) == horse


notEmpty pic = pic /= []

rectangular pic =
  notEmpty pic &&
  and [ length first == length l | l <-rest ]
  where
    (first:rest) = pic

height, width :: Picture -> Int

height = length
width = length . head

size :: Picture -> (Int,Int)

size pic = (width pic, height pic)



----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- Exercise 1 Define a function
-- picSize :: Picture -> (Int,Int)
-- which takes as input a picture with m rows and n columns, and returns
-- the size of the picture, which is the tuple (m, n).

picSize :: Picture -> (Int,Int)
picSize pict
        | notEmpty pict == False = (0,0)
        | rectangular pict == True = (width pict, height pict) 

-- Usage -------------------------------------
-- picSize horse





----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- Exercise 2 Define the function
-- rotate90 :: Picture -> Picture
-- which rotates a picture through 90◦ clockwise. For instance, rotating the
-- horse defined in module Picture should return the picture

-- import Data.List (transpose)

rotate90 :: Picture -> Picture
rotate90 = reverse . transpose

-- Usage -------------------------------------
-- printPicture horse
-- printPicture (rotate90 horse) =>90 degrees
-- printPicture (rotate90 (rotate90 horse)) =>180 degrees
-- printPicture (rotate90 (rotate90 (rotate horse))) =>270 degrees
-- printPicture (rotate90 (rotate90 (rotate90 (rotate90 horse)))) =>360 degrees





----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- Exercise 3 Define the polymorphic function
-- scaleList :: [a] -> Int -> [a]
-- which scales the input list by replicating every list element by the inte-
-- ger provided by the second argument. For example, scaleList "abc" 3
-- should return the result "aaabbbccc".

scaleList :: [a] -> Int -> [a]
scaleList [] _ = [] 
-- the default 
scaleList (x:xs) n = [x | i <- [1..n]] ++ (scaleList xs n)
-- to generate each element on i times and concatenate with the rest of the list 
-- ['a' | i<-[1..10]] -> will produce "aaaaaaaaaa"


-- Usage -------------------------------------
-- scaleList "abc" 3





----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- Exercise 4
-- definition of scaleList lst n
-- scaleList is defined in Exercise 3

-- set scaleRow equivalent to scaleList
scaleRow = scaleList

-- scale function takes 2 arguments:
-- pic of type Picture and n of type Int
-- scale returns a picture 
scale :: Picture -> Int -> Picture

-- Lambda abstraction \row -> (scaleRow row n)
-- map((\row -> (scaleRow row n)) pic) applies the scaleRow function to the pic argument
-- scaleList is used to generate each specific row on n times and concatenate with the rest of the list
scale pic n = (scaleList (map ( \row -> (scaleRow row n)) pic) n)

-- Variable exPic of type Picture
exPic :: Picture
exPic = [ "#.#",
          "..#"]

-- Variable scaledPic of type Picture stores the scaled picture
scaledPic :: Picture
scaledPic = scale exPic 2

-- Usage -------------------------------------
-- printPicture (scale horse 3)





----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- Exercise 5
-- Exercise 5
-- a)Define the list primes of all prime numbers with the sieve of Erathostenes.
-- x will refer to the first element in the list and xs (pronounced exes, as in the plural of x) refers to the remainder of the list.
-- primes is recursive. The list that it generates consists of the first element of the list passed into it and the output of primes when passed the remainder of the list after some filtering.
-- filter function expects two arguments. The first argument is a predicate and the second is a list. 
-- filter returns a new list consisting of the elements from the initial list that yield True when passed to the predicate.
-- The . operator composes two functions i.e. (f . g) x == f (g x).
primes (x:xs) = x : primes (filter ((/= 0) . (`mod` x)) xs)

-- b)Use primes to define the function prime n which returns the n-th prime number.
-- primes [2, 3, 4, 5, 6...] = 2 : primes [3, 5, 7, 9, 11...]            -> 1st prime number
--                           = 2 : 3 : primes [5, 7, 11, 13, 17...]      -> 2nd prime number
--                           = 2 : 3 : 5 : primes [7, 11, 13, 17, 19...] -> 3rd prime number
--                           = 2 : 3 : 5 : 7 : primes [11, 13, 17, 19, 23...] 
--                           = 2 : 3 : 5 : 7 : 11 : primes [13, 17, 19, 23, 29...]
-- prime function has to be supplied with a list of the positive integers starting at 2
-- !! (n-1) returns the element of the stream xs at index n-1,this is because the head of the stream has index 0.
prime n = primes [2..] !! (n-1)





----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- Exercise 6 (calculate triples based on constraints) From assignment
nextTriple :: (Int,Int,Int) -> (Int,Int,Int)
nextTriple (0,0,z) = (z+1,0,0)
nextTriple (0,x,y) = (x-1,0,y+1)
nextTriple (x,y,z) = (x-1,y+1,z)

triples :: [(Int,Int,Int)]
triples = (0,0,0):map nextTriple triples

----------------------------------------------------------------------------
-- a) If we compute the value of triples, then our program will run forever.
--    The list triples is an infinite list and if we want to display this value, then the program will run forever.
--    In this case we will need to force terminate it in order to finish the operation.

----------------------------------------------------------------------------
-- b) Return the n-th pitagorean tuple
pyth :: Int -> (Int, Int, Int)
pyth x = head (drop (x-1) (allPyth 1000))

allPyth :: Int -> [(Int, Int, Int)]
allPyth i = [(a, b, c) | a <- [0..i], b <- [0..a], c <- [0..i], (a^2) + (b^2) == (c^2)]

----------------------------------------------------------------------------
-- c) Indicate the first 6 Pythagorean tuples
firstSix :: [(Int,Int,Int)]
firstSix = take 6 (allPyth 5)





----------------------------------------------------------------------------
----------------------------------------------------------------------------
----------------------------------------------------------------------------
-- Exercise 7: Consider the Shape data type for geometric shapes which was defined in the Lecture Notes.
data Shape  = Circle Float | Rectangle Float Float | Triangle Float Float Float
-- Define the function
perimeter :: Shape -> Float
-- which takes as input a list of geometric shapes and returns its perimeter.
-- We know that in Math to calculate the Circle's perimeter is 2 * π * R
perimeter (Circle r) = 2 * pi * r
-- We know that in Math to calculate the Rectangle's perimeter is width + height + width + height, so we can say that it is 2 * height * width
perimeter (Rectangle h w) = 2 * h * w
-- We know that in Math to calculate the Triangle's perimeter is a + b +c
perimeter (Triangle a b c)= a + b + c

-- Usage
-- perimeter (Circle 10)
-- perimeter (Rectangle 5 12)
-- perimeter (Triangle 5 7 12)