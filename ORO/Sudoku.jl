#rules:
#the numbers 1 to 9 must appear in each 
#1->3x3 square
#2->row
#3->column
import Pkg
Pkg.add("JuMP")
Pkg.add("GLPK")
using JuMP, GLPK
#x[i,j,k]=1 if and only if cell (i,j) has number k
sudoku=Model(GLPK.Optimizer)
@variable(sudoku, x[i=1:9, j=1:9, k=1:9], Bin)
for i = 1:9, j = 1:9  # Each row and each column
    # Sum across all the possible digits
    # One and only one of the digits can be in this cell, 
    # so the sum must be equal to one ( we are talking about binary elements)
    @constraint(sudoku, sum(x[i,j,k] for k=1:9) == 1)
end
for ind = 1:9  # Each row, OR each column
    for k = 1:9  # Each digit
        # Sum across columns (j) - row constraint
        @constraint(sudoku, sum(x[ind,j,k] for j=1:9) == 1)
        # Sum across rows (i) - column constraint
        @constraint(sudoku, sum(x[i,ind,k] for i=1:9) == 1)
    end
end
for i = 1:3:7, j = 1:3:7, k = 1:9
    # i is the top left row, j is the top left column
    # We'll sum from i to i+2, e.g. i=4, r=4, 5, 6 (3x3)
    @constraint(sudoku, sum(x[r,c,k] for r=i:i+2 for c=j:j+2) == 1)
end

"""
all from above can be written more clean, like:
@variable(sudoku, x[1:9, 1:9, 1:9], Bin)
@constraints(sudoku, begin
    ## Constraint 1 - Only one value appears in each cell
    cell[i in 1:9, j in 1:9], sum(x[i, j, :]) == 1
    ## Constraint 2 - Each value appears in each row once only
    row[i in 1:9, k in 1:9], sum(x[i, :, k]) == 1
    ## Constraint 3 - Each value appears in each column once only
    col[j in 1:9, k in 1:9], sum(x[:, j, k]) == 1
    ## Constraint 4 - Each value appears in each 3x3 subgrid once only
    subgrid[i=1:3:7, j=1:3:7, val=1:9], sum(x[i:i + 2, j:j + 2, val]) == 1
end)
"""
# An example
init_sol = [ 3 1 0 0 5 8 0 0 4;
             0 0 9 3 2 0 0 0 0;
             0 2 5 1 0 4 0 9 0;
             0 0 0 0 0 0 3 8 9;
             0 0 8 0 0 0 5 0 0;
             5 4 6 0 0 0 0 0 0;
             0 8 0 2 0 3 6 5 0;
             0 0 0 0 7 1 4 0 0;
             7 0 0 4 8 0 0 2 1]
for i = 1:9, j = 1:9
    # If the space isn't empty
    if init_sol[i,j] != 0
        # Then the corresponding variable for that digit
        # and location must be 1
        @constraint(sudoku, x[i,j,init_sol[i,j]] == 1)
    end
end
# We are now ready to solve the problem if and only if the status is optimal https://jump.dev/JuMP.jl/v0.21.1/solutions/
optimize!(sudoku)
status = termination_status(sudoku)
#println(status)
#if isequal(status,"OPTIMAL") or status == "OPTIMAL" -> none of them is working
# Extract the values of x
x_val = value.(x)
# Create a matrix to store the solution
sol = zeros(Int,9,9)  # 9x9 matrix of integers
for i in 1:9, j in 1:9, k in 1:9
    # Integer programs are solved as a series of linear programs
    # so the values might not be precisely 0 and 1. We can just
    # round them to the nearest integer to make it easier
    if round(x_val[i,j,k]) == 1
        sol[i,j] = k
    end
end
# Display the solution
println(sol)
#end
