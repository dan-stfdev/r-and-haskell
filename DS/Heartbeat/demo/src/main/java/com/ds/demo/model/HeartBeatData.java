package com.ds.demo.model;

import java.util.ArrayList;
import java.util.List;

public class HeartBeatData {
	
	List<String> ips = new ArrayList<String>();
	Integer currentIndex = 0;
	
	public HeartBeatData() {
		
	}
	
	public List<String> getIps() {
		return ips;
	}

	public void setIps(List<String> ips) {
		this.ips = ips;
	}
	
	public Integer getCurrentIndex() {
		return currentIndex;
	}
	
	public void setCurrentIndex(Integer currentIndex) {
		this.currentIndex = currentIndex;
	}
	
}
