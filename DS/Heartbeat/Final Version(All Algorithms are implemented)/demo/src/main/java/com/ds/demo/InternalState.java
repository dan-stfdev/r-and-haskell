package com.ds.demo;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class InternalState {
	
	public static Map<String, AtomicInteger> machinesStates = new ConcurrentHashMap<>();
	public static final InternalState state = new InternalState();
	
	private InternalState() {
		// Singleton
	}
	
	public static InternalState getInstance() {
		return state;
	}
	
	public static void initialize(Map<String, AtomicInteger> machines) {
		InternalState.machinesStates = machines;
	}
	
	public static void update(String ip) {
		AtomicInteger counter = InternalState.machinesStates.get(ip);
		if (counter != null) {
			counter.incrementAndGet();
			InternalState.machinesStates.put(ip, counter);
			System.out.println("Updated ip: " + ip + ", at counter value: " + counter.get());
		} else {
			machinesStates.put(ip, new AtomicInteger(1));
			System.out.println("Updated ip: " + ip + ", at counter value: " + 1);
		}
	}
	
	public static void remove(String ip) {
		if (InternalState.machinesStates.get(ip) != null) {
			InternalState.machinesStates.remove(ip);
			System.out.println("Removed ip: " + ip);
		}
	}

	public Map<String, AtomicInteger> getMachinesStates() {
		return InternalState.machinesStates;
	}

	public void setMachinesStates(Map<String, AtomicInteger> machinesStates) {
		InternalState.machinesStates = machinesStates;
	}

	
}
