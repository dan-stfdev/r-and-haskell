package com.chandy.lamport.snapshot;

import java.util.HashMap;

public class ServerProperties {
	
	static ServerProperties ServerPropertiesObject = null;
	
	static HashMap<String, Integer> processId = new HashMap<>();
	static String[] servers = {"10.128.0.2", "10.128.0.3", "10.128.0.4"};
	static final int numberOfProcesses = servers.length;
	static String suffix = "";
	
	private ServerProperties(){
		int number = 0;
		for(String serverName : servers){
			processId.put(serverName, number++);
		}
	}
	
	static ServerProperties getServerPropertiesObject(){
		if(ServerPropertiesObject == null){
			new ServerProperties();
		}
		return ServerPropertiesObject;
	}
	
}
