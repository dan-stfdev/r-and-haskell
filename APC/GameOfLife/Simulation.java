
//youtube -> https://youtu.be/ncRA1OAbMmk
//github -> https://github.com/Tyjoh/GameOfLifeSimulator/tree/7c8249b6532483de9325ed09aff7b4ba2f01bba3

public class Simulation {

	//properties for the board (width, height, board itself - 2D array)
    int width;
    int height;
    int[][] board; //here you can change the "int" to "boolean", and also below, where this function is used (0 is false and 1 is true)
    //since the board contains cells that are either dead (0), either alive (1)
    
    //the constructor, which creates the board, according to the provided values for
    //width and height (object created in main())
    public Simulation(int width, int height) {
        this.width = width;
        this.height = height;
        this.board = new int[width][height];
    }

    //print the board
    public void printBoard() {
        System.out.println("---"); //delimit the new board from the one that is displayed before (earlier generation)
        //go through the matrix with 2 for loops (starting from the top, we travel the matrix), 
        //but on the horizontal line, not vertically (for each row = height (y), we travel each cell/column = width (x))
        //the "|" are delimiters for the board, on the left (beginning of the row) and on the right (end of the row)
        for (int y = 0; y < height; y++) { 
            String line = "|"; //this variable (line) is used to display characters for the board
            //this "|" is the line which appears at the beginning of each row
            for (int x = 0; x < width; x++) {
                if (this.board[x][y] == 0) { //if the cell is DEAD 
                    line += "."; //display .
                } else { //else, if the cell is ALIVE 
                    line += "*"; //display *
                }
            }
            line += "|"; //this "|" is for the end of the row
            System.out.println(line); //print the characters for this row and start the next line
        }
        System.out.println("---\n"); //after the entire board is printed, leave some space delimited for the next board
    }

    //these "set" functions are used when the board is created, to set some cells alive/dead
    public void setAlive(int x, int y) { //set alive the cell that is at point with coordinates (x,y)
        this.board[x][y] = 1;
    }

    public void setDead(int x, int y) { //set dead the cell that is at point with coordinates (x,y)
        this.board[x][y] = 0;
    }

    //for each cell that is at the coordinates (x,y), count all the neighbors 
    public int countAliveNeighbours(int x, int y) {
    	
    	//representation of the neighbors, for a cell(x,y)
    	/*
    	 (x-1, y-1)  (x, y-1)  (x+1, y-1)
    	 (x-1, y)     (X, Y)   (x+1, y)
    	 (x-1, y+1)  (x, y+1)  (x+1, y+1)
    	 */
    	
        int count = 0; //start the counting of alive neighbors
        //go counting from top to bottom, horizontally (from left to right)

        //UP (on top of the cell)
        count += getState(x - 1, y - 1);
        count += getState(x, y - 1);
        count += getState(x + 1, y - 1);

        //on left and right of the cell (same line)
        count += getState(x - 1, y);
        count += getState(x + 1, y);

        //DOWN (under the cell)
        count += getState(x - 1, y + 1);
        count += getState(x, y + 1);
        count += getState(x + 1, y + 1);

        return count; //return the number of alive neighbors
    }

    //get the state of the cell which is at the coordinates (x,y)
    //this will be used to find the state of the neighbors
    //if it's outside the board, consider it dead
    public int getState(int x, int y) {
        if (x < 0 || x >= width) { //outside of the board, on the left or right, horizontally speaking
            return 0;
        }

        if (y < 0 || y >= height) { //outside of the board, too up or down, vertically speaking
            return 0;
        }

        return this.board[x][y]; //if the cell is inside the board, return its state
    }

    //compute next generations (next boards)
    public void step() {
        int[][] newBoard = new int[width][height];

       //go through the matrix with 2 for loops (starting from the top, we travel the matrix), as explained above
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                int aliveNeighbours = countAliveNeighbours(x, y); //get the number of alive neighbors

                if (getState(x, y) == 1) { //if the cell (x,y) is ALIVE
                    if (aliveNeighbours < 2) { //if it has less than 2 alive neighbors (under-population)
                        newBoard[x][y] = 0; //make it dead
                    } else if (aliveNeighbours == 2 || aliveNeighbours == 3) { //if it has exactly 2 or 3 alive neighbors
                        newBoard[x][y] = 1; //keep it alive
                    } else if (aliveNeighbours > 3) { //if it has more than 3 neighbors (over-population)
                        newBoard[x][y] = 0; //make it dead
                    }
                } else { //ELSE, if the cell (x,y) is DEAD
                    if (aliveNeighbours == 3) { //if it has exactly 3 neighbors
                        newBoard[x][y] = 1; //make it alive
                    }
                }

            }
        }

        this.board = newBoard; //make a new board for the next generation
    }

    public static void main(String[] args) {
        Simulation simulation = new Simulation(8, 5); //create a board of size 8x5

        //set some cells alive
        simulation.setAlive(2, 2);
        simulation.setAlive(3, 2);
        simulation.setAlive(4, 2);

        //the following lines can be replaced and put in a for, to count the generations
        
        simulation.printBoard(); //print the current board

        simulation.step(); //make the "computations" for the next generation

        simulation.printBoard();

        simulation.step();

        simulation.printBoard();

    }

}
