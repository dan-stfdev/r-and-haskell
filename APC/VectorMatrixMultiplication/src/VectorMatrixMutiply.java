import java.util.ArrayList;
import java.util.List;

public class VectorMatrixMutiply {
	
	public static int inputIndex = 0;
	
	public static void main(String[] args) {

		// Input 1
//		int[][] matrix = {{1,2,1,3},{1,-1,2,-2},{4,3,2,1},{1,2,3,4}};
//		int[] vector = {1, 2, 3, 4};
		
		// Input 2
		int[][] matrix = {{1, 2, 1},{0, 1, 0},{2, 3, 4}};
		int[] vector = {2, 6, 1};
		
		// Add delays and crate processors
		int[][] delayedMatrix = constructMatrixWithDelays(matrix, vector, vector.length - 1);
		List<Procesor> procs = createProcessors(matrix);
		
		// Execute algorithm
		while(VectorMatrixMutiply.inputIndex < vector.length * 2 - 1) { // Stop condition
			
			System.out.println(System.lineSeparator() + "___=== Step number:" + (inputIndex + 1) + " ===___");
			
			// Propagate the input
			propagateInput(vector, delayedMatrix, procs, VectorMatrixMutiply.inputIndex);

			// Step function
			procs.forEach(Procesor::step);
			
			// Print out the results
			procs.forEach(e -> System.out.print("P" + procs.indexOf(e) + "->" + e + System.lineSeparator()));
		}
		
	}

	// Get element from matrix and get element from vector and assign it to the last processor
	// Pass the element from the vector from one processor to another
	private static void propagateInput(int[] vector, int[][] delayedMatrix, List<Procesor> procs, int inputIndex) {
		for (int i = 0; i < procs.size(); i++) {
			procs.get(i).mIn = delayedMatrix[i][inputIndex];
			procs.get(i).vIn = i < procs.size()-1 ? procs.get(i + 1).vOut : inputIndex < vector.length ? vector[inputIndex] : 0;
		}
		
		VectorMatrixMutiply.inputIndex++;
	}

	// Create the number of processors that are needed
	private static List<Procesor> createProcessors(int[][] matrix) {
		List<Procesor> procs = new ArrayList<>();
		for (int i = 0; i < matrix.length; i++) {
			Procesor proc = new Procesor();
			procs.add(proc);
		}
		return procs;
	}

	// Add the delays to the initial matrix (delays are represented by 0)
	private static int[][] constructMatrixWithDelays(int[][] matrix, int[] vector, int delay) {
		int[][] delayedMatrix = new int[matrix.length][matrix.length + vector.length - 1];
		for (int i = 0; i < matrix.length; i++) {
			System.arraycopy(matrix[i], 0, delayedMatrix[i], delay, matrix[i].length);
			delay--;
		}
		return delayedMatrix;
	}
	
}